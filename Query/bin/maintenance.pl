#!/usr/bin/perl

#use Data::Dumper;

use warnings;
use strict;
use utf8;

use Dancer;

any [qw/get post/] => qr/.*/ => sub {
    return <<'EOF';
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content/type" content="text/html;charset=utf-8">
<title>SeLaQ</title>
</head>
<body>
<h1>Omlouváme se.</h1>
<p>
Právě probíhá údržba služby. Zkuste se připojit později.
</body>
</html>
EOF
};

dance();
