package Query;
use Dancer;

set port         => 5125; # H.S. 30.1.2016

# use Data::Dumper;

use SeLaQ::SQL;
use SeLaQ::DBInfo;

our $VERSION = '0.4';

get '/' => sub {
    my $address = request()->address // 'UNKNOWN';
    info 'Connection from ' . $address;
    template 'select_layer';
};

get '/query' => sub {
    my $dbinfo = 'SeLaQ::DBInfo'->new;
    my $error_types;
    $error_types->{$_} = $dbinfo->error_types($_) for qw(a b);
    template 'query', { layer => param('layer'), error_types => $error_types };
};

get '/doc' => sub {
    my $lang = param('lang');
    send_error('Invalid language') unless -f "doc/$lang/index.tt";
    template "doc/$lang/index", {}, { layout => undef };
};


any [qw/get post/] => '/result' => sub {
    return redirect '/' if param('new_query');

    get_nodes(\ my %nodes);
    return template('empty') unless %nodes;

    my $context_size = param('context');
    my $sql = 'SeLaQ::SQL'->new( start_layer  => $nodes{0}{layer},
                                 show_layer   => param('show_layer'),
                                 context_size => $context_size
                               );

    my $error = $sql->process_nodes(%nodes);
    send_error($error) if $error;
    $sql->uniq(%nodes);

    my $query = $sql->serialize;
    info join ' ', params();
    info $query unless 'development' eq config->{environment};
    $sql->check;
    # debug Dumper $sql;
    return template('error', { messages => $sql->error }) if $sql->error;

    my $db = $sql->{db};
    if ('development' eq config->{environment}) {
        $db->profile(8);
        $db->trace(1);
    }

    my $result = $db->prepare($query);
    eval {
        # Do not issue any warnings, Dancer would catch them and return 500
        local $result->{PrintError};

        $result->execute;
        1;
    } or do {
        my $error = $@ =~ 'timeout' ? 'Timeout' : 'SQL Error: <pre>'. $@ . '</pre>';
        return template('error', { messages => [$error] });
    };

    my @results;
    while (my $r = $result->fetchrow_array) {
        $r =~ s/&/\&amp;/g;
        $r =~ s/</\&lt;/g;
        $r =~ s=\[!--\[(.*?)]--!]=<td nowrap><b>$1</b><td nowrap>=g;
        push @results, [$r];
    }
    $result->finish;
    $db->disconnect;

    template('result', { results => \@results });

};


get '/dancer' => sub {
    template 'dancer';
};


=head1 get_nodes($nodes)

Parse the parameters into the hash reference $nodes.

=cut

sub get_nodes {
    my $nodes = shift;
    for my $node (grep /^node[0-9]+$/, params()) {
        $node =~ /([0-9]+)/;
        my ($num, $param) = ($1, param($node));

        $nodes->{$num} = { value => $param,
                           from  => param("from$num"),
                           rel   => param("rel$num"),
                           layer => param("h_layer$num"),
                           attr  => param("attr$num.0"),
                           op    => param("op$num.0"),
                           error => param("err$num"),
                         };
        my $ref = param("ref$num");
        $nodes->{$num}{value} = $ref if defined $ref and $ref ne 'x';

        unless (defined $nodes->{$num}{value}
                and length $nodes->{$num}{value}) {
            $nodes->{$num}{value} = '.*';
            $nodes->{$num}{op}    = 're';
        }
    }
}


=head1 AUTHOR

(c) Jan Stepanek, 2012

=cut

__PACKAGE__
