use Test::More ( tests => 4 );
use strict;
use warnings;

my @path = split m{/}, $0;
pop @path;
my $dir = join '/', @path, '..';
ok (-f "$dir/views/locale/lang.tt", "lang.tt exists");

my %languages;
for my $file (<$dir/views/locale/*.tt>) {
    my ($language) = $file =~ m{/([^/]*)\.tt$} or next;
    undef $languages{$language} unless 'lang' eq $language;
}

open my $LANG_TT, '<', "$dir/views/locale/lang.tt" or die $!;
my $selected_lang;
while (<$LANG_TT>) {
    if (/\blocale\s*=\s*['"]([^'"]+)['"]/) {
        $selected_lang = $1;
    }
}
ok (exists $languages{$selected_lang}, "selected language exists");

for my $lang (keys %languages) {
    ok(-f "$dir/views/doc/$lang/index.tt", "doc for $lang exists");
}

