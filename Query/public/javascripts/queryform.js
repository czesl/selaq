num = 0;

function token_form (op_name, str_name, layer) {
    return '<select name="attr' + op_name + '">' +
              '<option value="token">Token</option>' +
                (layer == 'w' ? ' ' :
                 '<option value="lemma">Lemma</option><option value="mtag">Mtag</option>'
                 ) + '</select>' +
           '<select name="op' + op_name + '" onchange="rel_change(this)">' +
              '<option value="eq">=</option>' +
              '<option value="ne">&ne;</option>' +
              '<option value="in">&in;</option>' +
              '<option value="ni">&notin;</option>' +
              '<option value="re">&#x223C;</option>' +
              '<option value="nr">&#x2241;</option>' +
              '<option value="sa">&#x2261;</option>' +
              '<option value="ns">&#x2262;</option>' +
            '</select> <input type="text" name="node' + str_name + '" size="24">' +
           '<select name="ref' + str_name + '" hidden><option value=x>x</option></select>' +
           '<input type="button" value="+" size="1" disabled="disabled">';
}

function rel_change (sel) {
    switch(sel.value) {
        case 'sa':
        case 'ns':
            var reflist_sel = $(sel).next().next();
            $('option', reflist_sel).remove();
            for (i=0; i<=num; i++) {
                reflist_sel.append('<option value='+i+'>'+i+'</option>');
            }

            $(sel).next().attr('hidden', 'hidden')
                  .next().removeAttr('hidden');
            break;
        default:
            $(sel).next().removeAttr('hidden')
                .next().attr('hidden', 'hidden').children().remove();
            
    }
}

function add_layer (layer, check) {
    $('input[value="' + layer + '"]:radio')
        .removeAttr('hidden').next().removeAttr('hidden');
    if (check) {
        $('input[value="' + layer + '"]:radio').attr('checked', 'checked');
    }
}

function undo () {
    // node0 cannot be removed
    if (! num) { return } 

    var node = $('input[name="node' + num + '"]');
    var rel  = $('input[name="rel' + num + '"]');
    if (rel.val() == 'l' || rel.val() == 'r') {
        from = $('input[name="from' + num + '"]').val();
        $('input[name="node' + from + '"]').parent('div')
            .find('input[value="' + String.fromCharCode(8592)
                  +    ',value="' + String.fromCharCode(8594)
                  + '"]:disabled')
            .removeAttr('disabled');
    }

    node.parent().parent().remove();
    num--;
}
