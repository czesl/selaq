#-*- cperl -*-
use warnings;
use strict;
use utf8;

use Test::More tests => 6;

use SeLaQ::SQL;

my $s = 'SeLaQ::SQL'->new(
                          layer => 'w',
                          context_size => 10);

ok($s->isa('SeLaQ::SQL'));

$s->sql('join w as x on node0.spk + 1 = x.spk');
$s->where('1=1');

my $compare = $s->attribute(0, 'token', 'w');

is($compare, 'node0.token', '->attribute');

$s->operation('token', 'eq', q{řek'}, $compare);

my $ser = $s->serialize;

like($ser, qr/\.spk - 10/);
like($ser, qr/\.spk \+ 10/);
like($ser, qr/ from w as node0 /);

like($ser, qr/ node0\.token = 'řek''' /);

$s->{db}->disconnect;
