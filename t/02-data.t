#-*- cperl -*-
use warnings;
use strict;

use Test::More tests => 102;

use XML::XSH2;

BEGIN {
    use_ok('SeLaQ::DBO');
}

use SeLaQ::DBO;

my $db = 'SeLaQ::DBO'->new;
ok(exists $db->{db});
is(ref $db->{db}, 'DBI::db');

xsh << 'XSH';
quiet ;
register-namespace c http://utkl.cuni.cz/czesl/ ;
XSH

open my $DATADIR, '<', 'loaded.wlc' or die $!;
chomp ($XML::XSH2::Map::datadir = <$DATADIR>);

my $count;
my %attr_table = (w => [qw/w doc para/],
                  a => [qw/w doc para s error lex mtag/],
                  b => [qw/w doc para s error link lex mtag/]);
for my $layer (qw/w a b/) {
    for my $file (grep /\.$layer\.xml/, glob $XML::XSH2::Map::datadir) {
        xsh "open '$file'";
        for my $attr (@{ $attr_table{$layer} }) {
            xsh "\$c = count(//c:$attr"
                . ($layer eq 'w' ? '[not(c:dt)]' : q'')
                . ')';
            {
                no warnings 'once';
                $count->{$layer}{$attr} += $XML::XSH2::Map::c;
            }
        }
    }
}

for my $layer (qw/w a b/) {
    for my $attr (@{ $attr_table{$layer} }) {
        my $qstring = "select count(1) from $layer";
        if ($attr eq 'w') {
            $qstring .= ' where dt is false' if $layer eq 'w';
        } else {
            $qstring .= "_$attr";
        }
        my $query;
        ok($query = $db->prepare($qstring));
        is(ref $query, 'DBI::st');
        ok($query->execute);
        my ($result) = $query->fetchrow_array;
        cmp_ok($result, '==', $count->{$layer}{$attr}, "$layer/$attr");
        ok($query->finish);
    }
}


xsh << 'XSH';
    $c = 0 ;
    for my $bfile in { grep /\.b\.xml/, glob $datadir } {
        my $bdoc := open $bfile ;
        my $path = xsh:subst($bfile, '/[^/]*$', '/') ;

            # Check for non-existent targets.
        my $afile = concat($path, $bdoc/c:ldata/c:head/c:references/c:reffile[@name="adata"]/@href) ;
        my $adoc := open $afile ;
        my $ahash := hash @id $adoc//c:w ;
        for $bdoc//c:edge[count(xsh:lookup('ahash', substring-after(c:from, '#')))=0] $c = $c + 1 ;
    }
XSH
my $query = $db->prepare('select count(1) from
                    (select distinct e from b_edge where "from" is null) as e');
$query->execute;
my ($result) = $query->fetchrow_array;
cmp_ok($result, '==', $XML::XSH2::Map::c, 'Empty from');
$query->finish;


xsh << 'XSH';
    $c = 0 ;
    for { grep /\.b\.xml/, glob $datadir } {
        open (.) ;
        for //c:edge[count(c:to)=0 and name(..)='para'] $c = $c + 1 ;
}
XSH
$query = $db->prepare('select count(1) from
                      (select distinct e from b_edge where "to" is null) as e');
$query->execute;
($result) = $query->fetchrow_array;
cmp_ok($result, '==', $XML::XSH2::Map::c, 'Empty to');
$query->finish;


xsh << 'XSH';
    $c = 0 ;
    for { grep /\.b\.xml/, glob $datadir } {
        open (.) ;
        for //c:edge[count(c:from)>1] $c = $c + 1 ;
}
XSH
$query = $db->prepare('select count(1) from
                          (select e, count(1) from
                              (select distinct e, "from" from b_edge) as edge
                           group by e
                           having count(1)>1) as c');
$query->execute;
($result) = $query->fetchrow_array;
cmp_ok($result, '==', $XML::XSH2::Map::c, 'Multiple from');
$query->finish;


xsh << 'XSH';
    $c = 0 ;
    for { grep /\.b\.xml/, glob $datadir } {
        open (.) ;
        for (//c:para/c:edge[count(c:to)>1] | //c:w/c:edge[c:to]) $c = $c + 1 ;
}
XSH
$query = $db->prepare('select count(1) from
                          (select e, count(1) from
                              (select distinct e, "to"
                                   from b_edge) as edge
                              group by e
                              having count(1)>1) as c');
$query->execute;
($result) = $query->fetchrow_array;
cmp_ok($result, '==', $XML::XSH2::Map::c, 'Multiple to');
$query->finish;


xsh << 'XSH';
    $c = 0 ;
    $max = 0 ;
    for { grep /\.a\.xml/, glob $datadir } {
        open (.) ;
        for (//c:w/c:lex) {
            $count = count(c:mtag) ;
            if ($count > $max) {
                $max = $count ;
                $c = 1;
            } elsif ($count = $max) {
                $c = $c + 1 ;
            }
        }
    }
XSH
$query = $db->prepare('select max(count) from (select count(1) from a_mtag group by lex) as mtags_per_lex');
$query->execute;
($result) = $query->fetchrow_array;
cmp_ok($result, '==', $XML::XSH2::Map::max, 'mtags per lex');
$query->finish;
$query = $db->prepare('select count(1) from (select count(1) from a_mtag group by lex having count(1)=?) as max_mtags');
$query->execute($result);
($result) = $query->fetchrow_array;
cmp_ok($result, '==', $XML::XSH2::Map::c, 'count max mtags');
$query->finish;


xsh << 'XSH';
    $c = 0 ;
    $max = 0 ;
    for { grep /\.a\.xml/, glob $datadir } {
        open (.) ;
        for (//c:w) {
            $count = count(c:lex) ;
            if ($count > $max) {
                $max = $count ;
                $c = 1;
            } elsif ($count = $max) {
                $c = $c + 1 ;
            }
        }
    }
XSH
$query = $db->prepare('select max(count) from (select count(1) from a_lex group by w) as lexes_per_w');
$query->execute;
($result) = $query->fetchrow_array;
cmp_ok($result, '==', $XML::XSH2::Map::max, 'lexes per w');
$query->finish;
if ($result) {
    $query = $db->prepare('select count(1) from (select count(1) from a_lex group by w having count(1)=?) as max_lexes');
    $query->execute($result);
} else { # no lexes present, should count all w's with zero lexes
    $query = $db->prepare('select count(1) from a');
    $query->execute;
}
($result) = $query->fetchrow_array;
cmp_ok($result, '==', $XML::XSH2::Map::c, 'count max lexes');
$query->finish;


ok($db->disconnect);
