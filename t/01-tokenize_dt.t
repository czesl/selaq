#-*- cperl -*-
use warnings;
use strict;

use Test::More;

BEGIN {
    use_ok('SeLaQ::Loader');
}

use utf8;


use SeLaQ::Loader qw/tokenize_dt/;
my $NS = 'NNOOSSPPAACCEE';

# is_deeply([tokenize_dt('')],
#           []);


is_deeply(tokenize_dt(''),
          '');

is_deeply([tokenize_dt('a')],
          [qw/a/]);

is_deeply([tokenize_dt('a b')],
          [qw/a b/]);

is_deeply([tokenize_dt('a b.')],
          ['a', "b$NS", '.']);

is_deeply([tokenize_dt('abc (def ghi)')],
          ['abc', "($NS", 'def', "ghi$NS", ')']);

is_deeply([tokenize_dt('12. 1. získala firma I5P 4,7 dolaru za akcii.')],
          ["12$NS", '.', "1$NS", '.', 'získala', 'firma', 'I5P',
           '4,7', 'dolaru', 'za', "akcii$NS", '.']);

is_deeply([tokenize_dt('Žluťoučký kůň (dospělí prominou...), kterého jsem (?) potkal...')],
          ['Žluťoučký', 'kůň', "($NS", 'dospělí', "prominou$NS",
          "…$NS", ")$NS", ',', 'kterého', 'jsem', "($NS", "?$NS", ')',
           "potkal$NS", '…']);

done_testing(8);
