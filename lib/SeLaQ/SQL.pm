package SeLaQ::SQL;

=head1 SeLaQ::SQL

Module that handles the construction of the SQL query from the form
parameters.

=cut

use warnings;
use strict;

use SeLaQ::DBO;


=head1 'SeLaQ::SQL'->new(timeout => 30_000,
                         context_size => 25,
                         show_layer   => 'w',
                         start_layer  => 'w')

The constructor. Creates a new SeLaQ::SQL object representing the
query. Using the named arguments some attributes can be changed, but
default values exist for all of them.

=cut

sub new {
    my ($class, %args) = @_;
    my $self = { db => 'SeLaQ::DBO'->new };

    my $timeout = $args{timeout} // 10_000;
    $self->{db}->prepare('set statement_timeout to ?')->execute($timeout);

    $self->{context_size} = $args{context_size} // 25;
    $self->{show_layer}   = $args{show_layer}   // 'w';

    $args{start_layer} //= 'w';
    $self->{select} = "from $args{start_layer} as node0";
    $self->{nodes}{$args{start_layer}} = [0];
    $self->{group} = [];

    bless $self, $class;
}


sub DESTROY {
    $_[0]->{db}->disconnect;
}

=head1 $sql->where(@w)

Adds conditions specified in @w into the WHERE clause.

=cut

sub where {
    my $self = shift;
    push @{ $self->{where} }, @_;
}


=head1 $sql->sql(@s)

Add the given statements after the JOIN part but before the "context"
part of the query (see C<< $sql->context >>). Usually used for joining
non-node tables.

=cut

sub sql {
    my $self = shift;
    push @{ $self->{sql} }, @_;
}


=head1 $sql->add_join($node, $layer, @c)

=head1 $sql->add_join(@c)

Adds a JOIN statement to the query with conditions taken from @c. If a
node (and a layer) is not specified (two or less arguments), the word
C<join> is not inserted into the JOIN clause: use this form to
further specify the ON part of the previous JOIN.

=cut

sub add_join {
    my $self = shift;
    if (@_ > 2) {
        my ($node, $layer) = (shift, shift);
        push @{ $self->{join} }, 'join ', @_;
    } else {
        push @{ $self->{join} }, grep defined, @_;
    }
}


# used by attribute
sub _search_lemma {
    my ($self, $layer, $prefix, $node) = @_;
    $self->sql("join $layer\_lex as $prefix$node on node$node.spk = $prefix$node.w");
    push @{ $self->{group} }, "$prefix$node.spk";
}

sub _search_mtag {
    my ($self, $layer, $prefix_l, $prefix_t, $node) = @_;
    $self->sql("join $layer\_lex  as $prefix_l$node on $prefix_l$node.w   = node$node.spk"
            . " join $layer\_mtag as $prefix_t$node on $prefix_t$node.lex = $prefix_l$node.spk");
    push @{ $self->{group} }, "$prefix_l$node.spk", "$prefix_t$node.spk";
}


=head1 $sql->attribute($node, $attr, $layer, $rel, $op)

Creates joins for more complicated attributes located in separate
tables (lemma, tag).

=cut

sub attribute {
    my ($self, $node, $attr, $layer, $rel, $op) = @_;
    return "node$node.token" if 'token' eq $attr;

    if ('lemma' eq $attr) {
        $self->error('No lemma on w layer!') if 'w' eq $layer;

        # Do not join _lex on starting node nor for SAME relations
        $self->_search_lemma($layer, 'lex', $node)
            unless 'start' eq $rel and $op =~ /^(?:sa|ns)$/;
        return "lex$node.lemma";
    }

    if ('mtag' eq $attr) {
        $self->error('No mtag on w layer!') if 'w' eq $layer;

        # Do not join _lex on starting node nor for SAME relations
        $self->_search_mtag($layer, 'lex', 'tag', $node)
            unless 'start' eq $rel and $op =~ /^(?:sa|ns)$/;
        return "tag$node.tag";
    }
}


=head1 $sql->operation($attr, $op, $val, $cmp, $layer)

Adds the WHERE parts to the query. The $cmp should be the result of
C<< $sql->attribute >>.

=cut

my %operations = (eq => '=',
                  ne => '!=',
                  re => '~',
                  nr => '!~',
                 );

sub operation {
    my $self = shift;
    my ($attr, $op, $val, $compare, $layer) = @_;
    if (exists $operations{$op}) {
        $self->where("$compare $operations{$op} " . $self->{db}->quote($val));

    } elsif ($op =~ /^(?:in|ni)$/) {
        my $rel = $op eq 'in' ? 'in' : 'not in';
        my @values = split /(?<!\\)\|/, $val;
        s/\\\|/|/g for @values;

        $self->where("$compare $rel ("
                    . join(',', map $self->{db}->quote($_), @values) . ')');
    } elsif ($op =~ /^(?:sa|ns)$/) {
        $self->check([$val, 'w', $attr]);

        if ('token' eq $attr) {
            my $rel = $op eq 'sa' ? '=' : '!=';
            $self->where("$compare $rel node$val.$attr");

        } else {
            my ($node) = $compare =~ /(?:lex|tag)([0-9]+)/;
            my ($vlayer) = map $_->[0],
                           grep { grep $_ == $val, @{ $_->[1] } }
                           map [$_, $self->{nodes}{$_}],
                           qw/a b/;

            if ('lemma' eq $attr) {
                $self->where(('sa' eq $op ? q() : 'not ') .
                         "exists ( select lemma from $vlayer\_lex as slemma$val\_$node
                                   where slemma$val\_$node.w = node$val.spk
                                   intersect
                                   select lemma from $layer\_lex as dlemma$val\_$node
                                   where dlemma$val\_$node.w = node$node.spk)");

            } elsif ('mtag' eq $attr) {
                $self->where(('sa' eq $op ? q() : 'not ') .
                         "exists ( select tag from $vlayer\_mtag as stag$val\_$node
                                   join $vlayer\_lex as slex$val\_$node
                                       on slex$val\_$node.spk = stag$val\_$node.lex
                                   where slex$val\_$node.w = node$val.spk
                                   intersect
                                   select tag from $layer\_mtag as dtag$val\_$node
                                   join $layer\_lex as dlex$val\_$node
                                       on dlex$val\_$node.spk = dtag$val\_$node.lex
                                   where dlex$val\_$node.w = node$node.spk)");
            }
        }
    }
}


=head1 $sql->check

=head1 $sql->check(@conditions)

If conditions are specified, they are just stored for later use. If no
condition is specified, all the stored conditions are checked. Each
condition should have the form

    [ $node, $layer, $attr ]

The non-existence of the $attr on the $layer is reported if the $node
belongs to the $layer.

=cut

sub check {
    my $self = shift;
    if (@_) {
        push @{ $self->{check} }, @_;
    } else {
        for my $check (@{ $self->{check} // [] }) {
            my ($node, $layer, $attr) = @$check;
            if ('token' ne $attr
                and grep $node eq $_, @{ $self->{nodes}{$layer} // [] }) {
                $self->error("Attribute $attr does not exist on layer $layer (node $node)!");
            }
        }
    }
}


=head1 $sql->process_nodes(%nodes)

Populate the $sql object from the given query $nodes.

=cut

sub process_nodes {
    my ($self, %nodes) = @_;

    # Build the layer to node map, needed to solve forward pointing SAME relations
    for my $layer (qw/w a b/) {
        push @{ $self->{nodes}{$layer} }, grep $layer eq $nodes{$_}{layer}, keys %nodes;
    }

    my %processed = ();
    while (keys %nodes != keys %processed) {
        my %free = %nodes;
        delete @free{keys %processed};
        my $error;
        my $node_id = (sort { $a <=> $b } keys %free)[0];
        my $node    = $nodes{$node_id};
        my $rel     = $node_id ? $node->{rel} : 'start';
        my $layer   = $node->{layer};
        my $from    = $node->{from};
        $error      = $node->{error} if exists $node->{error} and defined $node->{error};

        if ($rel) {
            my $compare = $self->attribute($node_id, $node->{attr}, $layer, $rel, $node->{op});

            if ($rel =~ /^[du]$/) {
                my $edge = 'u' eq $rel   ? $layer
                         : 'w' eq $layer ? 'a'
                         : 'b';
                my @direction = qw(from to);
                @direction = reverse @direction if 'd' eq $rel;

                $self->add_join($node_id, $layer,
                                "$edge\_edge as edge$node_id on edge$node_id.$direction[0]=node$from.spk
                                 join $layer as node$node_id on node$node_id.spk=edge$node_id.$direction[1]");
                push @{ $self->{group} }, "edge$node_id.e";

                if ($error) {
                    $self->sql("join $edge\_error as error$node_id on edge$node_id.e=error$node_id.e");
                    $self->where("error$node_id.tag=" . $self->{db}->quote($error) . " and edge$node_id.$direction[0]=node$from.spk")
                }

            } else {
                $self->add_join($node_id, $layer, "$layer as node$node_id on") if $node_id;
            }

            $self->operation(@{ $node }{qw/attr op value/}, $compare, $node->{layer});

            if ($rel =~ /^[rl]$/) {
                $self->add_join("node$node_id.spk = node$from.spk "
                                . ('r' eq $rel ? '+' : '-') . " 1");

            } elsif ($rel =~ /^(?:ll|rr)$/) {
                my @cmps = 'll' eq $rel ? qw(< + >) : qw(> - <);
                $self->add_join("node$node_id.spk " . (shift @cmps) . " node$from.spk");
                $self->where("node$node_id.spk " . (shift @cmps) . " 5 "
                             . (shift @cmps) . " node$from.spk");

            } elsif ($rel !~ /^(?:d|u|start)$/) {
                return('Unknown relation');
            }
        }
        undef $processed{$node_id};
    }
    return;
}


=head1 $sql->uniq(@nodes)

Two "boxes" cannot match the same node.

=cut

sub uniq {
    my ($self, %nodes) = @_;
    for my $layer (qw/w a b/) {
        my @nodes_on_layer = grep $nodes{$_}{layer} eq $layer, keys %nodes;
        for my $n1 (0 .. $#nodes_on_layer - 1) {
            for my $n2 ($n1 + 1 .. $#nodes_on_layer) {
                $self->where("node$nodes_on_layer[$n1].spk "
                            . "!= node$nodes_on_layer[$n2].spk");
            }
        }
    }
}

=head1 $sql->debug(@messages)

If messages are specified, they are stored for later use. If no
messages are specified, all the stored messages are returned.

=cut

sub debug {
    my $self = shift;
    $self->_error_or_debug('debug', @_);
}


=head1 $sql->error(@messages)

Similar to C<debug>.

=cut

sub error {
    my $self = shift;
    $self->_error_or_debug('error', @_);
}


# Used by debug and error.
sub _error_or_debug {
    my $self = shift;
    my $type = shift;
    die "Unknown type" unless grep $_ eq $type, qw(error debug);
    if (@_) {
        push @{ $self->{$type} }, @_;
    } else {
        return @{ $self->{$type} // []};
    }
}


=head1 $sql->context

Creates the "context" part of the query, after the "sql" and before
WHERE. It is used to search for context, i.e. neighbouring nodes.

=cut

sub context {
    my $self  = shift;
    my $layer = $self->{show_layer};
    my $context_size = $self->{context_size};
    my $node  = $self->{nodes}{$layer}[0];
    unshift @{ $self->{where} }, "where c.doc = node$node.doc";
    return "join $layer as c on c.spk"
        . " between node$node.spk - $context_size"
        . " and node$node.spk + $context_size";
}


=head1 $sql->serialize

Returns the SQL query as a string.

=cut

sub serialize {
    my $self = shift;
    my $layer = $self->{show_layer};
    return join ' ', 'select distinct string_agg(case when c.spk in ('
                         . join(',', map "node$_.spk", @{ $self->{nodes}{$layer} }) . ') '
                         . q{then '[!--[' || c.token || ']--!]' }
                         . q{else c.token end, ' ' order by c.spk)},
                     $self->{select},
                     @{ $self->{join} // [] },
                     @{ $self->{sql} // []},
                     $self->context,
                     join(' and ', @{ $self->{where} // []}),
                     'group by', join ',',
                         @{ $self->{group}},
                         map "node$_.spk", map @{ $self->{nodes}{$_} // [] }, qw/w a b/;
}


=head1 AUTHOR

(c) Jan Stepanek, 2012

=cut

__PACKAGE__
