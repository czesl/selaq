#-*- cperl -*-
package SeLaQ::DBO;

=head1 SeLaQ::DBO

Basic OO interface to SeLaQ database.

=cut

#use Data::Dumper;

use warnings;
use strict;

use DBI;

use constant DB_NAME => 'czesl';


=head1 'SeLaQ::DBO'->new

The constructor. Creates a new SeLaQ::DBO object, connects to the
database.

=cut

sub new {
    my $class = shift;
    my $db = 'DBI'->connect('dbi:Pg:dbname=' . DB_NAME, '', '',
                            { AutoCommit => 0,
                              PrintError => 1,
                              RaiseError => 1,
                              pg_enable_utf8 => 1,
                            }) or die "Cannot connect\n";
    my $self = {db => $db};
    bless $self, $class;
}


=head1 $dbo->profile($x)

Sets the profile level of the underlying database to $x.

=cut

sub profile {
    my $self = shift;
    $self->{db}->{Profile} = shift;
}


=head1 $dbo->commit

=head1 $dbo->do

=head1 $dbo->disconnect

=head1 $dbo->prepare

=head1 $dbo->quote

=head1 $dbo->trace

Methods delegated to the underlying database.

=cut

my $dbi_methods = join q(),
    map {
        my $sub = 'sub XXX {
            my $self = shift;
            $self->{db}->XXX(@_);}';
        $sub =~ s/XXX/$_/g;
        $sub;
    } qw/commit do disconnect prepare quote trace/;

eval $dbi_methods;


=head1 AUTHOR

(c) Jan Stepanek, 2012

=cut

__PACKAGE__
