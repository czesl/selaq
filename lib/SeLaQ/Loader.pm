package SeLaQ::Loader;

use warnings;
use strict;
use feature qw/switch say/;
use open qw/:utf8 :std/;

use parent 'SeLaQ::DBO';

use XML::LibXML;

use Data::Dumper;

use Exporter 'import';
our @EXPORT_OK = qw/load/;
push @EXPORT_OK, 'tokenize_dt'; # for testing

use constant PML_NS  => 'http://utkl.cuni.cz/czesl/';

############################################################################

my $NOSPACE = 'NNOOSSPPAACCEE';

sub new {
    my $class = shift;
    my $self = $class->SUPER::new;
    bless $self, $class;
}

sub reset_timer {
    my $self = shift;
    $self->{timer} = time;
}

sub timer {
    my $self = shift;
    info("Timer @_: ", time - $self->{timer});
}


=head1 load (files)

The main subroutine: loads given files to the db.

=cut

sub load {
    my $db = 'SeLaQ::Loader'->new;
    $db->do('set statement_timeout to 0');
    $db->drop_tables;


    my @count;

    $db->reset_timer;
    push @count, $db->load_w(grep /\.w\.xml$/, @_);
    $db->timer('w');

    push @count, $db->load_higher('a', grep /\.a\.xml$/, @_);

    push @count, $db->load_higher('b', grep /\.b\.xml$/, @_);

    for my $i (1 .. $#count) {
        die "Invalid number of files loaded.\n" if $count[$i-1] != $count[$i];
    }

    $db->vacuum;
    $db->disconnect;
}


sub info {
    print {*STDERR} 'INFO: ', @_, "\n";
}


sub vacuum {
    my $db = shift;
    $db->reset_timer;
    local $db->{db}{AutoCommit} = 1;
    $db->do('vacuum full analyze');
    $db->timer('Vacuum');
}


=head1 id (element)

Helper function; returns the C<id> attribute of the element.

=cut

sub id {
    return $_[0]->getAttribute('id');
}

=head1 children (element, name)

Helper function; returns the list of children named C<name> of the
element.

=cut

sub children {
    return $_[0]->getChildrenByLocalName($_[1]);
}

=head1 child (element, name)

Helper function; returns the first child named C<name> of the element
(used for elements with just one child with the given name).

=cut

sub child {
    return $_[0]->getChildrenByLocalName($_[1])->[0];
}


=head1 db -> drop_tables

Drops tables in the data to be created and loaded later.

=cut

# All tables dropped here to avoid cascading conflicts.
sub drop_tables {
    my $db = shift;
    $db->do('drop table if exists b_link');
    for my $layer (qw/a b/) {
        $db->do("drop table if exists $layer\_error");
        $db->do("drop table if exists $layer\_edge");
        $db->do("drop table if exists $layer\_edge_id");
        $db->do("drop table if exists $layer\_mtag");
        $db->do("drop table if exists $layer\_lex");
    }
    $db->do('drop table if exists w_alt');
    $db->do('drop table if exists w');
    $db->do('drop table if exists w_para');
    $db->do('drop table if exists w_doc');
    for my $layer (qw/a b/) {
        $db->do("drop table if exists $layer");
        $db->do("drop table if exists $layer\_s");
        $db->do("drop table if exists $layer\_para");
        $db->do("drop table if exists $layer\_doc");
    }
}

###################  W  ###################


=head1 db -> setup_w

Creates the tables related to w-layer, prepares insert statements.

=cut

sub setup_w {
    my $db = shift;

    $db->do('create table w_doc (spk serial primary key,
                                 id varchar,
                                 file varchar)');

    $db->do('create table w_para (spk serial primary key,
                                  id varchar,
                                  doc int)');

    $db->do('create table w (spk serial primary key,
                             par int,
                             doc int,
                             id varchar,
                             token varchar,
                             no_space_after bool,
                             dt bool,
                             start int)');

    $db->do('create table w_alt (w_rf int,
                                 token varchar)');

    $db->commit;

    $db->{ins_doc} = $db->prepare('insert into w_doc values (default, ?, ?)');
    $db->{ins_par} = $db->prepare('insert into w_para values (default, ?, '
                                . '(select last_value from w_doc_spk_seq))');

    $db->{ins_alt} = $db->prepare('insert into w_alt values ('
                                . '(select last_value from w_spk_seq), ?)');

    $db->{ins} = $db->prepare('insert into w values (default, '
                              . '(select last_value from w_para_spk_seq), '
                              . '(select last_value from w_doc_spk_seq), '
                              . '?, ?, ?, ?, ?)');
}


=head1 tokenize_dt (dt_token)

Pre-printed text is not tokenized in the original XML. This function
normalizes it a bit and returns the text tokenized. Special value
C<$NOSPACE> is used for tokens with no space after.

=cut

sub tokenize_dt {
    my $token = shift;
    $token =~ s/\.\.\./chr 8230/ge;   # Unicode "..."
    my @chars = split //, $token;

    my $subtoken = '';
    my @tokens;
    for my $idx (0 .. @chars) {
        my $char = $chars[$idx] // ' ';
        if ($char =~ /[[:alnum:]]/
            and (substr($subtoken, -1) =~ /[[:alnum:]]/
                 or not length $subtoken
                 or $subtoken =~ /[0-9][.,]$/)) {
            $subtoken .= $char;
        } elsif ($char =~ /[[:alnum:]]/) {
            push @tokens, $subtoken . $NOSPACE;
            $subtoken = $char;
        } elsif ($char =~ /\s/) {
            push @tokens, $subtoken;
            $subtoken = '';
        } elsif ($char =~ /[.,]/
                 and substr($subtoken, -1) =~ /[0-9]/
                 and $chars[$idx+1] =~ /[0-9]/) {
            $subtoken .= $char;
        } elsif ($char =~ /[[:punct:]]/ and length $subtoken) {
            push @tokens, $subtoken . $NOSPACE;
            $subtoken = $char;
        } elsif ($char =~ /[[:punct:]]/) {
            $subtoken = $char;
        } else {
            die "Unknown $char at [$subtoken]!";
        }
    }
    return @tokens;
}


=head1 db -> add_w_constraints

Adds foreign key constrains to w tables.

=cut

sub add_w_constraints {
    my $db = shift;
    $db->reset_timer;
    $db->do('alter table w_para add constraint w_para2doc
             foreign key (doc) references w_doc(spk)');
    $db->do('alter table w add constraint w2para
             foreign key (par) references w_para(spk)');
    $db->do('alter table w add constraint w2doc
             foreign key (doc) references w_doc(spk)');
    $db->timer('add_w_constraints');
}


=head1 db -> load_w_w (w)

Load the given w-element into the db.

=cut

sub load_w_w {
    my ($db, $w) = @_;
    my $id = id($w);
    my @alts = map $_->textContent,
                  @{ children($w, 'token') };
    my $token = shift @alts;

    my $start = child($w, 'original_position')
                      ->getAttribute('from');

    my $no_space_after = child($w, 'no_space_after');
    $no_space_after = $no_space_after
                      ? $no_space_after->textContent
                      : 0;

    my @dt;
    my $dt = child($w, 'dt');
    if ($dt and $dt->textContent == 1) {
        @dt = tokenize_dt($token);
        undef $token;
        $no_space_after = 0;

        for my $dt (@dt) {
            my $no_space_after
              = ($dt =~ s/$NOSPACE// ? 1 : 0);
            $db->{ins}->execute($id, $dt,
                                $no_space_after, 1, undef);
        }
        return;
    } else {

        $db->{ins}->execute($id, $token, $no_space_after, 0, $start);

        $db->{ins_alt}->execute_array({ArrayTupleFetch => sub {
                                           my $alt = shift @alts;
                                           return defined $alt ? [$alt]
                                                               : undef;
                                       } } );
    }
}


=head1 db -> load_w_para (para)

Loads given para into the db.

=cut

sub load_w_para {
    my ($db, $para) = @_;
    $db->{ins_par}->execute(id($para));
    $db->load_w_w($_) for @{ $db->{xpc}->find('p:w', $para) };
}


=head1 db -> load_w_doc (doc)

Loads given PML document into the db.

=cut

sub load_w_doc {
    my ($db, $doc) = @_;
    $db->{ins_doc}->execute(id($doc), $db->{file});
    $db->load_w_para($_) for @{ $db->{xpc}->find('p:para', $doc) };
}

=head1 db -> load_w (w-files...)

Loads given w-files into the given db.

=cut

sub load_w {
    my $db = shift;
    $db->setup_w;

    my $file_count = 0;

    while ($db->{file} = shift) {
        info("Loading w from $db->{file}.");
        $file_count++;
        my $xml = 'XML::LibXML'->load_xml(location => $db->{file});
        $db->{xpc} = 'XML::LibXML::XPathContext'->new;
        $db->{xpc}->registerNs('p', PML_NS);
        $db->load_w_doc($_)
            for (@{ $db->{xpc}->find('/p:wdata/p:doc',
                                     $xml->documentElement)
                });
    }
    $db->add_w_constraints;

    $db->commit;
    info("Loaded $file_count w-files.");
    return $file_count;
} # load_w


###################  A, B  ###################

=head1 db -> create_tables (layer)

Creates the tables related to the higher layer.

=cut

sub create_tables {
    my ($db, $layer) = @_;
    $db->do("create table $layer\_doc (spk  serial,
                                       id   varchar,
                                       file varchar)");

    $db->do("create table $layer\_para (spk serial,
                                        id  varchar,
                                        doc int)");

    $db->do("create table $layer\_s (spk  serial,
                                     id   varchar,
                                     para int)");

    $db->do("create table $layer (spk   serial,
                                  token varchar,
                                  id    varchar,
                                  s     int,
                                  para  int,
                                  doc   int)");

    $db->do("create table $layer\_lex (spk serial,
                                       id varchar,
                                       lemma varchar,
                                       w int)");
    $db->do("create table $layer\_mtag (spk serial,
                                        id varchar,
                                        tag varchar(15),
                                        lex int)");

    $db->do("create table $layer\_edge_id (spk serial,
                                           id  varchar)");
    $db->do(qq{create table $layer\_edge (e      int,
                                          "from" int,
                                          "to"   int)});

    if ($layer eq 'a') {
        $db->do("create table a_error (e   int,
                                       tag varchar )");
    }

    if ($layer eq 'b') {
        $db->do('create table b_error (e   int,
                                       tag varchar,
                                       spk serial,
                                       id  varchar)');

        $db->do('create table b_link (err int,
                                      lnk int)');
    }
    $db->commit;
}

=head1 db -> setup_higher (layer)

Prepare insert statements for the higher layer.

=cut

sub setup_higher {
    my ($db, $layer) = @_;
    $db->create_tables($layer);

    $db->{ins} = $db->prepare("insert into $layer values (default, ?, ?,"
                   . join(', ',
                          map "(select last_value from $layer\_$_\_spk_seq)",
                              qw/s para doc/) . ')');

    $db->{ins_doc} = $db->prepare("insert into $layer\_doc values
                                       (default, ?, ?)");

    $db->{ins_par} = $db->prepare("insert into $layer\_para values (default, ?,
                                (select last_value from $layer\_doc_spk_seq))");

    $db->{ins_s} = $db->prepare("insert into $layer\_s values (default, ?,
                              (select last_value from $layer\_para_spk_seq))");

    $db->{ins_e_id}
        = $db->prepare("insert into $layer\_edge_id values (default, ?)");

    $db->{ins_e} = $db->prepare("insert into $layer\_edge values(?, ?, ?)");

    if ($layer eq 'a') {
        $db->{ins_error} = $db->prepare('insert into a_error values(?, ?)');
    } else {
        $db->{ins_error} = $db->prepare('insert into b_error
                                          values(?, ?, default, ?)');
    }

    $db->{ins_lex} = $db->prepare("insert into $layer\_lex
                          values (default, ?, ?,
                                 (select last_value from $layer\_spk_seq))");

    $db->{ins_mtag} = $db->prepare("insert into $layer\_mtag
                           values(default, ?, ?, ?)");
    $db->{sel_last_lex}
        = $db->prepare("select last_value from $layer\_lex_spk_seq");

    $db->{sel_last_e}
        = $db->prepare("select last_value from $layer\_edge_id_spk_seq");

    my $lower = $layer eq 'a' ? 'w' : 'a';
    $db->{id_map_low} = $db->prepare("select id, spk from $lower
                    where doc = (select last_value from $layer\_doc_spk_seq)");
    $db->{id_map} = $db->prepare("select id, spk from $layer
                    where doc = (select last_value from $layer\_doc_spk_seq)");

    if ($layer eq 'b') {
        $db->{ins_link} = $db->prepare('insert into b_link values(
                                   (select last_value from b_error_spk_seq),
                                   (select spk from b_edge_id where id = ?))');
    }
} # setup_higher


=head1 db -> add_higher_constraints (layer)

Adds constraints to higher tables.

=cut

sub add_higher_constraints {
    my ($db, $layer) = @_;
    $db->reset_timer;

    my @primary = qw{doc para s edge_id lex mtag};
    push @primary, 'error' if $layer eq 'b';
    $_ = "$layer\_$_" for @primary;
    for my $table (@primary, $layer) {
        $db->do("alter table $table add primary key (spk)");
    }

    my @references = qw{ para    doc     doc
                         s       para    para
                         !       s       s
                         !       para    para
                         !       doc     doc
                         lex     w       !
                         mtag    lex     lex
                         edge    e       edge_id
                         edge    "from"  !lower
                         edge    "to"    !
                         error   e       edge_id
                         b_link  err     error
                         b_link  lnk     edge_id
                    };

  REFERENCE:
    while (my ($from, $column, $to) = splice @references, 0, 3) {
        if ($from eq '!') {
            $from = $layer;
        } elsif ($from !~ /_/) {
            $from = "$layer\_$from";
        }

        next REFERENCE if $from =~ /_/ and $layer ne substr $from, 0, 1;

        if ($to eq '!') {
            $to = $layer;
        } elsif ($to eq '!lower') {
            $to = $layer eq 'a' ? 'w' : 'a';
        } else {
            $to = "$layer\_$to";
        }
        $db->do("alter table $from add constraint ${from}2$to
                   foreign key ($column) references $to(spk)");
    }

    # Needed to speed up SAME_AS and NOT_SAME.
    $db->do("create index on $layer\_lex (w)");
    $db->do("create index on $layer\_mtag (lex)");

    $db->timer("add_$layer\_constraints");
}


=head1 db -> prepare_edges (context, where, w-id)

Prepare list of edges to be later inserted into the db.

=cut

sub prepare_edges {
    my ($db, $xpc, $where, $wid) = @_;
    my (@edges, @errors, @links);
    my $edges = $xpc->find('p:edge', $where);
    for my $edge (@$edges) {
        my $edge_id = id($edge);
        $db->{ins_e_id}->execute($edge_id);
        $db->{sel_last_e}->execute;
        my ($last_e) = $db->{sel_last_e}->fetchrow_array;
        my $froms = children($edge, 'from');
        my $tos   = children($edge, 'to');
        my $ers   = children($edge, 'error');

        my $er_counter = 0;
        for my $er (@$ers) {
            my $tag    =    child($er, 'tag')->textContent;
            my $links  = children($er, 'link');
            my $err_id = "${edge_id}er${er_counter}";
            push @errors, [$last_e, $tag, $err_id];
            push @links, [$_->textContent] for @$links;
            $er_counter++;
        }

        push @$froms, undef unless @$froms; # no from in edge
        for my $from (@$froms) {
            my $from_text;
            if (ref $from) { # not a stub
                $from_text = $from->textContent;
                $from_text =~ s/.*#//;
            }

            # $wid is undef for edges in para
            push @edges, [$last_e, $from_text, $wid];

            my $i;
            for my $to (@$tos) {
                my $to_text = $to->textContent;
                push @edges, [$last_e, $from_text, $to_text];
            }
        }
    }
    return \@edges, \@errors, \@links;
} # prepare_edges


=head1 db -> process_edges (edges_ref)

Transforms XML ids into DB ids.

=cut

# Run the query just once per document, not for each edge.
sub process_edges {
    my $db = shift;
    for my $edge (@{ $db->{edges} }) {
        my ($id, $from, $to) = @$edge;
        if (defined $from) {
            if (not exists $db->{from}{$from}) {
                $db->{from} = {};
                $db->{id_map_low}->execute;
                while (my @tuple = $db->{id_map_low}->fetchrow_array) {
                    $db->{from}{$tuple[0]} = $tuple[1];
                }
                $db->{id_map_low}->finish;
            }
            $edge->[1] = $db->{from}{$from};
        }

        if (defined $to) {
            if (not exists $db->{to}{$to}) {
                $db->{to} = {};
                $db->{id_map}->execute;
                while (my @tuple = $db->{id_map}->fetchrow_array) {
                    $db->{to}{$tuple[0]} = $tuple[1];
                }
                $db->{id_map}->finish;
            }
            $edge->[2] = $db->{to}{$to};
        }
    }
}


=head1 db -> load_mtags (mtag, mtag, ...)

Loads the mtags into the db.

=cut

sub load_mtags {
    my $db = shift;
    $db->{sel_last_lex}->execute;
    my $lex = $db->{sel_last_lex}->fetchrow_array;
    $db->{ins_mtag}->execute(id($_), $_->textContent, $lex) for @_;
}


=head1 db -> load_lex (lex)

Loads the lex into the db.

=cut

sub load_lex {
    my ($db, $lex) = @_;
    $db->{ins_lex}->execute(id($lex), child($lex, 'lemma')->textContent);
    $db->load_mtags(@{ $db->{xpc}->find('p:mtag', $lex) });
}


=head1 db -> load_higher_w (w)

Loads the w into the db.

=cut

sub load_higher_w {
    my ($db, $w) = @_;
    my $wid = id($w);
    my $token = child($w, 'token')->textContent;
    $db->{ins}->execute($token, $wid);

    $db->load_lex($_) for @{ $db->{xpc}->find('p:lex', $w) };

    my ($edg, $err, $lnk)
        = $db->prepare_edges($db->{xpc}, $w, $wid);
    push @{ $db->{edges} },  @$edg;
    push @{ $db->{errors} }, @$err;
    push @{ $db->{links} },  @$lnk;
}


=head1 db -> load_higher_s (s)

Loads the s into the db.

=cut

sub load_higher_s {
    my ($db, $s) = @_;
    $db->{ins_s}->execute(id($s));
    $db->load_higher_w($_) for @{ $db->{xpc}->find('p:w', $s) };
}


=head1 db -> load_higher_para (para)

Loads the para into the db.

=cut

sub load_higher_para {
    my ($db, $para) = @_;
    $db->{ins_par}->execute(id($para));
    $db->load_higher_s($_) for @{ $db->{xpc}->find('p:s', $para) };

    my ($edg, $err, $lnk)
        = $db->prepare_edges($db->{xpc}, $para, undef);
    push @{ $db->{edges} },  @$edg;
    push @{ $db->{errors} }, @$err;
    push @{ $db->{links} },  @$lnk;
}


=head1 db -> load_higher_doc (doc)

Loads the doc into the db.

=cut

sub load_higher_doc {
    my ($db, $doc) = @_;
    $db->{ins_doc}->execute(id($doc), $db->{file});
    $db->load_higher_para($_) for @{ $db->{xpc}->find('p:para', $doc) };
    $db->process_edges;
}


=head1 db -> load_higher (layer, files...)

Loads given a- or b-files into the given db.

=cut

sub load_higher {
    my ($db, $layer) = (shift, shift);
    die "w-layer not loaded\n" unless exists $db->{xpc};
    $db->setup_higher($layer);

    my $file_count = 0;
    while ($db->{file} = shift) {
        info("Loading $layer from $db->{file}.");
        $file_count++;
        my $xml = 'XML::LibXML'->load_xml(location => $db->{file});

        $db->{$_} = [] for qw/edges errors links/;

        $db->load_higher_doc($_)
            for @{$db->{xpc}->find('/p:ldata/p:doc', $xml->documentElement)};

        $db->{ins_e}->execute_array({ArrayTupleFetch =>
                                     sub { shift @{ $db->{edges} } } } );

        $db->{ins_error}->execute_array({ArrayTupleFetch => sub {
                                       my $values = shift @{ $db->{errors} };
                                       return unless defined $values;
                                       if ($layer eq 'a' ) {
                                           pop @$values;
                                       }
                                       return $values;
                                   } } );

        $db->{ins_link}->execute_array({ArrayTupleFetch =>
                                        sub { shift @{ $db->{links} } } } )
            if $layer eq 'b';
    }

    $db->add_higher_constraints($layer);

    # destroy select handles
    delete $db->{$_} for grep /^sel/, keys %$db;

    $db->commit;
    info("Loaded $file_count $layer-files.");
    return $file_count;
} # load_higher


=head1 AUTHOR

(c) Jan Stepanek, 2012

=cut

__PACKAGE__
