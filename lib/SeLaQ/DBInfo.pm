package SeLaQ::DBInfo;

=head1 SeLaQ::DBInfo

Get basic info from the database (e.g. error types).

=cut

use warnings;
use strict;

use SeLaQ::DBO;


=head1 'SeLaQ::DBInfo'->new

Returns the object.

=cut

sub new {
    my $class = shift;
    my $self = { db => 'SeLaQ::DBO'->new };
    bless $self, $class;
}


=head1 $dbinfo->error_types($layer)

Returns the possible error types for given layer (C<a> or C<b>) as an
array reference.

=cut

sub error_types {
    my ($self, $layer) = @_;
    die "Invalid layer $layer\n" unless $layer =~ /^[ab]$/;
    if (not exists $self->{error_types}{$layer}) {
        my $db = $self->{db};
        my $select = $db->prepare("select distinct tag from $layer\_error order by tag");
        $select->execute;
        $self->{error_types}{$layer} = [ map @$_, @{ $select->fetchall_arrayref } ];
    }
    return $self->{error_types}{$layer};
}


sub DESTROY {
    my $self = shift;
    $self->{db}->disconnect;
}

__PACKAGE__
