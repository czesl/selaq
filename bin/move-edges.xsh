# Transforms old format data (edges at the end of para) to the newer
# one (edges inside of w if possible).
quiet;
register-namespace c http://utkl.cuni.cz/czesl/ ;
for my $file in { @ARGV } {
    open $file ;
    my $h := hash @id //c:w ;
    for //c:para/c:edge {
        if (c:to[1]) {
            my $w = xsh:lookup('h', c:to[1]) ;
            if (not($w/c:edge)) {
                delete c:to[1] ;
                xmove . after $w/c:token ;
            } else {
                echo Already has edge @id ;
            }
        }
    }
    save :b ;
}

