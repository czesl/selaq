#!/usr/bin/perl
#-*- cperl -*-

=pod

Loads the data into the database. Called from the Makefile.

=cut

use warnings;
use strict;

use SeLaQ::Loader;

SeLaQ::Loader::load(@ARGV);
