# Some files begin with the filename repeated. This junk text should
# be removed.

register-namespace pml http://ufal.mff.cuni.cz/pdt/pml/ ;
register-namespace c   http://utkl.cuni.cz/czesl/ ;

my $dir = { $ARGV[0] } ;

for my $file in { glob "$dir/*.w.xml" } {
    open $file ;
    my $p = (//c:para)[position()=1][1<count(.//text()[xsh:matches(.,'_')])] ;
    if $p {
        echo :n $file {"\t"} ;
        for $p//c:token
            echo :n (.) ;
        echo ;

        my $wid = (//c:para)[1]/@id ;
        rm (//c:para)[1] ;
        save :b ;

        perl { $file =~ s/\.w\./.a./ } ;
        open $file;
        my $c = //c:para[@lowerpara.rf=concat('w#', $wid)] ;
        if $c {
            my $aid = $c/@id ;
            rm $c ;
            echo $aid at a ;
            save :b ;

            perl { $file =~ s/\.a\./.b./ } ;
            open $file;
            my $c = //c:para[@lowerpara.rf=concat('a#', $aid)] ;
            if $c {
                my $bid = $c/@id ;
                rm $c ;
                echo $bid at b ;
                save :b ;
            }
        }
    }
}
