#!/usr/bin/perl

=pod

Some files exist in several directories. Their IDs must be prefixed
with direcotry names to be unique across the whole corpus.

=cut

use warnings;
use strict;

my $dir = shift;

for my $file (glob "$dir/?/*/*.xml") {
    my ($num, $set) = $file =~ m(/(.)/(.)[^/]*/[^/]*$);
    open my $IN,  '<', $file or die $!;
    open my $OUT, '>', "$file.o" or die $!;

    while (<$IN>) {
        s/id="([wab]-)/id="$set$num$1/g;
        s/(<from>.#)([aw]-)/$1$set$num$2/g;
        s/<from>w-/<from>$set${num}w-/g;
        s/<token>w-/<token>$set${num}w-/g if m{<change\b} .. m{</change\b};
        s/<link>/<link>$set$num/g;
        s/<to>(.-)/<to>$set$num$1/g;
        s/(lower[^.]+\.rf=".#)(.)/$1$set$num$2/g;
        s/<edge id="/<edge id="$set$num/g;
        print {$OUT} $_;
    }

    close $OUT or die $!;
    rename $file, "$file~" or die $!;
    rename "$file.o", $file or die $!;

}
