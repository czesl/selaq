#!/usr/bin/perl

=pod

Creates the documentation from html templates. Called from the Makefile.

=cut


use warnings;
use strict;

my @lines;
my $toc_line;
my @toc;

while (<>) {
    push @lines, $_;
    if (my ($name, $level, $title) =
            m%<a name="([^"]+)"></a><h([2-9])>([^<]+)</h\2>%) {
        push @toc, { name  => $name,
                     level => $level,
                     title => $title};
    }
}

my $last_level = 2;
for (@lines) {
    if (1 + index $_, '<? toc ?>') {
        for my $toc_line (@toc) {
            print spaces($toc_line->{level});
            if ($toc_line->{level} > $last_level) {
                print '<ul>';
            } elsif ($toc_line->{level} < $last_level) {
                print '</ul>' x ($last_level - $toc_line->{level});
            }
            print '<li><a href="#', $toc_line->{name}, '">',
                $toc_line->{title}, '</a>';
            print "\n";
            $last_level = $toc_line->{level};
        }
    } else {
        print;
    }
}

sub spaces {
    return ' ' x (4 + 2 * $_[0]);
}
