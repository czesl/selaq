SHELL:=/bin/bash
TRY_DB:=psql czesl <<< '\q'
PRODUCTION_PORT:=5125
DANCER_ENV:=$$(if [[ $$HOSTNAME = chomsky.* ]] ; then echo '--environment=production --port='$(PRODUCTION_PORT) ; fi)

help:
	@echo Possible targets:
	@grep -E '^[a-z_]+:($$|[^=])' Makefile | cut -f1 -d:

start_db:
	case $$HOSTNAME in \
	     (weed) pg_ctl -D /backup/test/POSTGRES-DB start ;; \
	     (still) pg_ctl -D ~/postgres start ;; \
	     (clay.*) pg_ctl -D ~/.pg start ;; \
	     (chomsky.*) pg_ctl -D ~/POSTGRES start ;; \
	     (*) echo How to run postgres on $$HOSTNAME? >&2 ; exit 1 ;; \
	esac
	i=0; until ((i>10)) || $(TRY_DB) ; do sleep 1 ; let i++ ; done ; \
	((i<11)) || exit 1

stop_db:
	case $$HOSTNAME in \
	    (weed) pg_ctl -D /backup/test/POSTGRES-DB stop ;; \
	    (still) pg_ctl -D ~/postgres stop ;; \
	    (clay.*) pg_ctl -D ~/.pg stop ;; \
	    (chomsky.*) pg_ctl -D ~/POSTGRES stop ;; \
	    (*) echo How to stop postgres on $$HOSTNAME? >&2 ; exit 1 ;; \
	esac

start_web: link_exists check_db
	ps x | grep 'perl [Q]uery/bin/maintenance.pl' \
	    | (read pid rest; kill $$pid || echo No maintenance running >&2)
	Query/bin/app.pl $(DANCER_ENV)


stop_web:
	ps x | grep 'perl [Q]uery/bin/app.pl' | (read pid rest; kill $$pid)

check_db:
	$(TRY_DB) || $(MAKE) start_db

maintain:
	$(MAKE) stop_web || echo No web running >&2
	$(MAKE) stop_db || echo No db running >&2
	Query/bin/maintenance.pl $(DANCER_ENV) &

_load: check_db
	time perl -Ilib bin/load.perl $$(< loaded.wlc)

load:
	echo '../data2/*.xml' > loaded.wlc
	$(MAKE) _load

load-big:
	echo '../corpus/*.xml' > loaded.wlc
	$(MAKE) _load

load-small:
	echo '../morph/HRD_ZL_*.xml' > loaded.wlc
	$(MAKE) _load

load-morph:
	echo '../morph2/?/*/*.?.xml' > loaded.wlc
	$(MAKE) _load

test-loader:
	prove -I lib

test-dancer:
	cd Query ; prove -I lib

test: test-loader test-dancer

link_exists:
	cd Query/lib ; [[ -L SeLaQ ]] || ln -s ../../SeLaQ .

doc: $(foreach lang,$(wildcard doc/*/.),Query/views/$(lang)/index.tt)

Query/views/doc/%/index.tt: doc/%/index.tt
	mkdir -p Query/views/doc/$*
	perl bin/makedoc.pl $< > $@

.PHONY: help start_db stop_db start_web stop_web check_db load load-small test-loader test-dancer test doc
